// simple_int_set.cc
#include "simple_int_set.h"


void SortSet(int*array, int& size){
  int i,j,temp;
  for (i=0; i<size; i++){
    for (j=i; j<size; j++){
      if(array[i] > array[j]){
        temp      = array[i];
        array[i]  = array[j];
        array[j]  = temp;
      }
      else if(array[i] == array[j]){
        array[j] =  array[--size];
        j--;
      }
    }
  }
}

SimpleIntSet SimpleIntSet::Intersect(const SimpleIntSet& int_set) const{
  int i, j;
  bool check=false;

  for (i = 0; i < size_; i++){
    for (check = j = 0; j < int_set.size(); j++){
      if(values_[i] == int_set.values()[j]){
        check = true;
        break;
      }
    }
    if(check == false){
      (int)values_[i] = values_[--(int)size_];
      i--;
    }
  }
  SortSet((int*)values_, (int)size_);
  return int_set;
}
SimpleIntSet SimpleIntSet::Union(const SimpleIntSet& int_set) const{
  int i;

  for (i=size_; i< int_set.size(); i++){
    (int)values_[i]=int_set.values()[i+int_set.size()];
  }
  (int)size_+=int_set.size();
  SortSet((int*)values_, (int)size_);
  return int_set;
}
SimpleIntSet SimpleIntSet::Difference(const SimpleIntSet& int_set) const{
  int i, j;

  for (i=0; i<size_; i++){
    for (j=0; j<int_set.size(); j++){
      if(values_[i]==int_set.values()[j]){
        (int)values_[i]=values_[--(int)size_];
        j--;
      }
    }
  }
  SortSet((int*)values_, (int)size_);
  return int_set;
}


void SimpleIntSet::Set(const int* values, int size){
  SortSet((int*)values, size);
  (int*)values_  = (int*)values;
  size_    =size;
}


void ShowIntSet(SimpleIntSet& int_set){
	int i;

	cout<<"{ ";
	for(i=0; i< int_set.size(); i++)
	{
		cout<<int_set.values()[i]<<' ';
	}
	cout<<'}'<<endl;
}
