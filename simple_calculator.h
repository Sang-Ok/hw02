// simple_calculator.h

#ifndef _SIMPLE_CALCULATOR_H_
#define _SIMPLE_CALCULATOR_H_

class SimpleCalculator {
 public:
  SimpleCalculator(int value) : value_(value) {}

  void Add(double val);
  void Subtract(double val);
  void Multiply(double val);
  void Divide(double val);

  double value() const { return value_; }

 private:
  double value_;
};

#endif  // _SIMPLE_CALCULATOR_H_
