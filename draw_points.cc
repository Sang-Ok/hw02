// draw_points.cc

#include <iostream>
using namespace std;

struct Point {
  unsigned int x, y;
};

void GetPoints(Point* points,const int& num_points){
  int i,x,y;
  for(i=0;  i<num_points;  i++){
    cin>>x>>y;
    points[i].x=x;
    points[i].y=y;
  }
}

// Implement this function.
void DrawPoints(const Point* points, int num_points){
  int wide=0, leng=0,i,j,k;
  bool check;
//  /*test*/cout<<"TEST:: getinto fx"<<endl;
  for(i=0; i<num_points; i++){
    if(wide < points[i].x)  wide = points[i].x;
    if(leng < points[i].y)  leng = points[i].y;
  }
//  /*test*/cout<<"TEST:: (wide,leng)==("<<wide<<','<<leng<<')'<<endl;
  for(i=0; i<=leng; i++){
    for(j=0; j<=wide; j++){
      for(check=k=0; k<num_points; k++){
        if(points[k].y==i&&points[k].x==j)
          check=1;
      }
    if(check)	cout<<'*';
    else	cout<<'.';
    }
  cout<<endl;
  }
//  /*test*/cout<<"TEST::All Printed"<<endl;
}  
  
  
  

/*  int wide=0, leng=0,i,j,k;
  Point temp_point;
  for(i=0; i<num_points; i++){	//B4 draw, sort
    for(j=i; j<num_points; j++){
      if(points[j].y > points[i].y) continue;
      if(points[j].y ==points[i].y)
        if(points[j].x > points[i].x) continue;
      temp_point= points[i];
      points[i] = points[j];
      points[j] = temp_point;
    }
				//B4 draw, sizecheck
    if(wide < points[i].x)  wide = points[i].x;
    if(leng < points[j].y)  leng = points[i].y;
  }

  k=0				//Start drawing
  for(i=0; i<=wide; i++){
    for(j=0; j<=leng; j++){
      if(points[k]=={i,j})
        cout<<'*';
      else
        cout<<'.';
    }
  cout<<endl;
  }
*/


int main() {
  // Fill the main function.
  int num_points;
  std::cin>>num_points;
//  /*test*/cout<<"TEST:: get num point :"<<num_points<<endl;
  if(num_points <= 0)  return 1;
  Point* points = new Point[num_points];
//  /*test*/cout<<"TEST:: making points[] success"<<endl;
  GetPoints(points,num_points);
//  /*test*/cout<<"TEST:: getting points success"<<endl;
  DrawPoints(points,num_points);
//  /*test*/cout<<"TEST:: drawing func success"<<endl;
  delete[] points;
  return 0;
}
