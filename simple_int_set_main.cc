// simple_int_set_main.cc
#include <iostream>
#include "simple_int_set.h"
#include "simple_int_set.cc"


using namespace std;


int main(){
  int i, num,
	values1[100],values2[100];
  string op;
  SimpleIntSet s1, s2;

  for(i=0; cin>>num; i++) {values1[i]=num;}
  s1.Set(values1,i);

  cin>>num;
  cin>>op;
  cin>>num;

  for(i=0; cin>>num; i++) {values2[i]=num;}
  s2.Set(values2,i);

  if (op[0]=='+')
    s1.Union(s2);
  else if (op[0]=='-')
    s1.Difference(s2);
  else if (op[0]=='*')
    s1.Intersect(s2);

  ShowIntSet(s1);
  return 0;
}
