#include <iostream>
#include <string>
#include "simple_calculator.h"
#include "simple_calculator.cc"

using namespace std;

int main()
{
	string op;
	int num;
	cin >> num;
	SimpleCalculator cal(num);
	while (true)
	{
		cin >> op;
		if(op == "=")
			break;		
                cin >> num;
		if(op =="+")
			cal.Add(num);
		else if(op =="-")
			cal.Subtract(num);
		else if(op =="*")
			cal.Multiply(num);
		else if(op =="/")
			cal.Divide(num);
	}
	return 0;
}
