// radix_notation.cc

#include <string>
#include <iostream>

using namespace std;

// Implement this function.
string RadixNotation(unsigned int number, unsigned int radix);


int main() {
  while (true) {
    unsigned int number, radix;
    cin >> number >> radix;
    if (radix < 2 || radix > 36) break;
    cout << RadixNotation(number, radix) << endl;
  }
  return 0;
}

string RadixNotation(unsigned int number, unsigned int radix){
	int result;
	string str;
	for(number;  number != 0 ;  number/=radix)
	{
		result	=	number % radix;
		if(9 < result)
			result = result -10 +'a' -'0';
		str = (char)('0'+result)+str;

	}
	return str;
}

